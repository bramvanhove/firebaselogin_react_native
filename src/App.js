import React, { Component } from "react";
import * as firebase from 'firebase';
import { View, Text } from "react-native";
import { Button, Card, CardSection, Input, Spinner, Header } from "./components/common";
import LoginForm from "./screens/LoginForm";
import Navigation from "./navigation";



export class App extends Component{

    state={
        loggedin: null
    }

    componentWillMount(){
        //firebase config
            firebase.initializeApp({
                apiKey: "AIzaSyD4fOlzMiUV2dZ8aC9ZFLSwyJyWnXuGxA8",
                authDomain: "release-84096.firebaseapp.com",
                databaseURL: "https://release-84096.firebaseio.com",
                projectId: "release-84096",
                storageBucket: "release-84096.appspot.com",
                messagingSenderId: "997419042879"
            });
        
        
    }
        

    render(){
        return(
            <Navigation/>
        )
    }
}

export default App;
