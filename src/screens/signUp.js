import React, { Component } from 'react';
import * as firebase from 'firebase';
import { Text, View } from 'react-native';
import { Button, Card, CardSection, Input, Spinner } from "../components/common";


export default class LoginForm extends Component{

  state = { 
    name: '',
    email: '',
    emailRepeat : '',
    paswoord: '',
    paswoordRepeat: '',
    error: '',
    busy: true
    
  }

  onButtonSignUp(){
    this.setState({error: "", busy: true})
    const { email,emailRepeat, paswoord, paswoordRepeat, name } = this.state;

    if (email == emailRepeat && paswoord == paswoordRepeat && name) {

      firebase.auth().createUserWithEmailAndPassword(email, paswoord)
        .then(function(data){
          console.log('uid', data.user.uid)
          
          const date = new Date();

          firebase.database().ref('Users/' + data.user.uid).set({
              email: email,
              date: date,
              p: paswoord,
              name: name

          }).then((data)=>{
              //success callback
          this.setState({error: "user created"})
          }).catch((error)=>{
              //error callback
              console.log('error ' , error)
          })

        })
        .catch((e) => {
          console.log("e", e);
          this.setState({error: e + "", busy: false})
        })

    }else{
      this.setState({error: "vul alles korekt in", busy: false})
    }
    
  }

  componentDidMount(){
    this.setState({ busy: false })
  }

  button(){
    if (this.state.busy) {
      return <Spinner size="small"/>
    }

    return <Button onPress={this.onButtonSignUp.bind(this)}>Create User </Button>
  }

  render(){

    return(
      <Card >


        <CardSection>
          <Input 
            label={"Name:"}
            value={this.state.name}
            onChangeText={text => this.setState({name: text})}
          />
        </CardSection>

        <CardSection>
          <Input 
            label={"Email:"}
            value={this.state.email}
            onChangeText={text => this.setState({email: text.replace(/\s/g, '')})}
          />
        </CardSection>
        
        <CardSection>
          <Input 
            label={"Email Repeat:"}
            value={this.state.emailRepeat}
            onChangeText={text => this.setState({emailRepeat: text.replace(/\s/g, '')})}
          />
        </CardSection>

        <CardSection>
          <Input 
            secureTextEntry
            label={"Paswoord:"}
            value={this.state.paswoord}
            onChangeText={text => this.setState({paswoord: text})}
          />
        </CardSection>

        <CardSection>
          <Input 
            secureTextEntry
            label={"Paswoord Repeat:"}
            value={this.state.paswoordRepeat}
            onChangeText={text => this.setState({paswoordRepeat: text})}
          />
        </CardSection>

          <Text style={styles.error} >{this.state.error}</Text>

        <CardSection>

          {this.button()}
          
        </CardSection>
      </Card>
    )
  }
};

const styles = {
  error:{
    fontSize: 20,
    alignSelf: 'center',
    color: 'red',
    backgroundColor: "#ffffff"
  }
};

