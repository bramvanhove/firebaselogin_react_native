import React from 'react'
import { TextInput, Text, View } from 'react-native';

const Input = ({label, value, onChangeText, secureTextEntry}) => {
    const {inputStyle, labelStyle, viewStyle } = styles

    return(
        <View style={viewStyle}>
            <Text style={labelStyle}>{label}</Text>
            <TextInput 
            secureTextEntry={secureTextEntry}
            autoCorrect={false}
            value={value}
            onChangeText={onChangeText}
            style={inputStyle} />
        </View>
    )
};

const styles = {
  inputStyle: {
    color: '#050',
    paddingRight:5,
    paddingLeft:5,
    fontSize:18,
    height:40, width:500,
    flex:2,
    backgroundColor: "#f2f2f2"
  },
  labelStyle:{
      fontSize: 18,
      color: "#000",
    flex:1
  },
  viewStyle:{
    height:80, flex:1, flexDirection:'row',
    alignItems: 'center'
  }
};

export { Input }