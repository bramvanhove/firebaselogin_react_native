// Import libraries for making a component
import React from 'react';
import { ActivityIndicator, View } from 'react-native';

// Make a component
const Spinner = ({size}) => {

  return (
    <View style={styles.SpinnerStyle}>
      <ActivityIndicator size={size || 'large'} />
    </View>
  );
};

const styles = {
    SpinnerStyle:{
        flex: 1,
        justifyContent: 'center',
        alignSelf: 'center',
    }
};

// Make the component available to other parts of the app
export { Spinner };