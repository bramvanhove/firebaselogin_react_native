import React, { Component } from 'react';
import * as firebase from 'firebase';
import { Text, View } from 'react-native';
import { Button, Card, CardSection, Input, Spinner } from "./common";


export default class LoginForm extends Component{

  state = { 
    email : '',
    paswoord: '',
    error: '',
    busy: true
    
  }

  onButtonLogIn(){
    this.setState({error: "", busy: true})
    const { email, paswoord } = this.state;
    
    firebase.auth().signInWithEmailAndPassword(email, paswoord)
      .then(() => {
        this.setState({error: "u bent ingelogt"})
      })
      .catch((e) => {
        console.log("e", e);
        this.setState({error: e + "", busy: false})
      })
  }

  componentDidMount(){
    this.setState({busy: false})
  }

  button(){
    if (this.state.busy) {
      return <Spinner size="small"/>
    }

    return <Button onPress={this.onButtonLogIn.bind(this)}>Login </Button>
  }

  render(){

    return(
      <Card >
        <CardSection>
          <Input 
            label={"Email:"}
            value={this.state.email}
            onChangeText={text => this.setState({email: text})}
          />
        </CardSection>

        <CardSection>
          <Input 
            secureTextEntry
            label={"Paswoord:"}
            value={this.state.paswoord}
            onChangeText={text => this.setState({paswoord: text})}
          />
        </CardSection>

          <Text style={styles.error} >{this.state.error}</Text>

        <CardSection>

          {this.button()}
          
        </CardSection>
      </Card>
    )
  }
};

const styles = {
  error:{
    fontSize: 20,
    alignSelf: 'center',
    color: 'red',
    backgroundColor: "#ffffff"
  }
};

