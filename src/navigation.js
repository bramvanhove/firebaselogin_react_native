import { createSwitchNavigator, createAppContainer  } from 'react-navigation';
import LoginForm from './screens/LoginForm';
import Home from './screens/home';
import SignUp from './screens/signUp';

const AppNavigator = createSwitchNavigator({
    LoginForm: { screen: LoginForm },
    SignUp: {screen: SignUp },
    Home: { screen: Home }
});



export default createAppContainer(AppNavigator);